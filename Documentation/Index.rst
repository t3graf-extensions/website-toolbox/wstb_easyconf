..  include:: /Includes.rst.txt

..  _start:

========
Easyconf
========

:Extension key:
    wstb_easyconf

:Version:
    |release|

:Language:
    en

:Author:
   Mike Tölle

:Email:
    development@t3graf-media.de

:License:
    This document is published under the Open Content License
    available from https://www.openhub.net/licenses/opl.

:Rendered:
    |today|

----
Integrates Roman Büchler's EXT:easyconf into the Ext:website_toolbox.

Provides a module to easily configure main aspects from a website.

Once set up the module allows website owners without any TYPO3 knowledge to
configure main aspects from the website.

----

**Table of Contents**

..  toctree::
    :maxdepth: 2

    Introduction/Index
    Configuration/Index
    User/Index
    Tutorials/Index
    Changelog/Index
