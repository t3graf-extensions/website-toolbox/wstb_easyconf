<?php declare(strict_types=1);

/*
 * This file is part of the composer package buepro/typo3-easyconf.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

defined('TYPO3') or die('Access denied.');

$GLOBALS['TCA']['tx_easyconf_configuration']['ctrl'] = [
    'title' => 'LLL:EXT:wstb_easyconf/Resources/Private/Language/locallang.xlf:index.title',
    'label' => '',
    'tstamp' => 'tstamp',
    'crdate' => 'crdate',
    'cruser_id' => 'cruser_id',
    'delete' => 'deleted',
    'enablecolumns' => [
    ],
    'searchFields' => '',
    'iconfile' => 'EXT:wstb_easyconf/Resources/Public/Icons/Extension.svg',
    'rootLevel' => 0,
    'hideTable' => 1,
    'EXT' => [
        'easyconf' => [
            'dataHandlerAllowedFields' => '',
        ]
    ],
];
$GLOBALS['TCA']['tx_easyconf_configuration']['columns'] = [];
$GLOBALS['TCA']['tx_easyconf_configuration']['palettes'] = [];
//$GLOBALS['TCA']['tx_easyconf_configuration']['columns']
(function () {
    //debug($GLOBALS['TCA']['tx_easyconf_configuration']['columns'], 'Altes TCA');
    //$testArray = $GLOBALS['TCA']['tx_easyconf_configuration']['columns'];
    //$GLOBALS['TCA']['tx_easyconf_configuration']['columns'] = [];
    //debug($testArray, 'Zwischenspeicher Array');
    //debug($GLOBALS['TCA']['tx_easyconf_configuration']['columns'], 'Nach Löschung');
    //$GLOBALS['TCA']['tx_easyconf_configuration']['palettes'] = [];
    $tcaOverridesPathForPackage = ExtensionManagementUtility::extPath('wstb_easyconf')
        . 'Configuration/TCA/Easyconf';
    //debug($tcaOverridesPathForPackage); //die();
    if (!is_dir($tcaOverridesPathForPackage)) {
        return;
    }
    $files = scandir($tcaOverridesPathForPackage);
    if ($files === false) {
        return;
    }
    foreach ($files as $file) {
        if (is_file($tcaOverridesPathForPackage . '/' . $file)
            && ($file !== '.')
            && ($file !== '..')
            && (substr($file, -4, 4) === '.php')
        ) {
            // require $tcaOverridesPathForPackage . '/' . $file;
        }
    }
    //debug($GLOBALS['TCA']['tx_easyconf_configuration']['columns'], 'Neues TCA');
    //debug(array_diff($testArray, $testArray), 'Vergleich');
    //die();

})();
