<?php

/*
 * This file is part of the composer package buepro/typo3-easyconf.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use Buepro\Easyconf\Mapper\SiteConfigurationMapper;

defined('TYPO3_MODE') || die();

// Register fields

$GLOBALS['TCA']['tx_easyconf_configuration']['columns'] = array_replace_recursive(
    $GLOBALS['TCA']['tx_easyconf_configuration']['columns'],
    [
        'websiteTitle' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:websiteTitle',
            'displayCond' => 'FIELD:easyconf_show_all_properties:=:1',
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'websiteTitle',
            ],
        ],
        'websiteCopyright' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:websiteCopyright',
            'displayCond' => 'FIELD:easyconf_show_all_properties:=:1',
            'config' => [
                'type' => 'text',
                'renderType' => 't3editor',
                'format' => 'html',
                'rows' => 2,
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'sitepackage.owner.copyright',
            ],
        ],
        'owner_company' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_company',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:easyconf_show_all_properties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'sitepackage.owner.company',
            ],
        ],
        'owner_contact_name' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_name',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:easyconf_show_all_properties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'sitepackage.owner.contact_name',
            ],
        ],
        'owner_domain' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_domain',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:easyconf_show_all_properties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'sitepackage.owner.domain',
            ],
        ],
        'owner_contact_address' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_address',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:easyconf_show_all_properties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'sitepackage.owner.contact_address',
            ],
        ],
        'owner_contact_address_supplement' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_address_supplement',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:easyconf_show_all_properties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'sitepackage.owner.contact_address_supplement',
            ],
        ],
        'owner_contact_zip' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_zip',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:easyconf_show_all_properties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'sitepackage.owner.contact_zip',
            ],
        ],
        'owner_contact_city' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_city',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:easyconf_show_all_properties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'sitepackage.owner.contact_city',
            ],
        ],
        'owner_contact_country' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_country',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:easyconf_show_all_properties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'sitepackage.owner.contact_country',
            ],
        ],
        'owner_contact_phone' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_phone',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:easyconf_show_all_properties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'sitepackage.owner.contact_phone',
            ],
        ],
        'owner_contact_fax' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_fax',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:easyconf_show_all_properties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'sitepackage.owner.contact_fax',
            ],
        ],
        'owner_contact_email' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_email',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:easyconf_show_all_properties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'sitepackage.owner.contact_email',
            ],
        ],
        'owner_contact_office_hours' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_office_hours',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:easyconf_show_all_properties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'text',
                'cols' => 20,
                'rows' => 2,
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'sitepackage.owner.contact_office_hours',
            ],
        ],
    ]
);

$GLOBALS['TCA']['tx_easyconf_configuration']['palettes'] = array_replace_recursive(
    $GLOBALS['TCA']['tx_easyconf_configuration']['palettes'],
    [
        'paletteConfiguration' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:palette_configuration',
            //'description' => 'LL: Configuration description',
            'showitem' => 'websiteTitle, --linebreak--, websiteCopyright',
        ],
        'paletteOwner' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:palette_owner',
            'showitem' => 'owner_company, owner_contact_name, --linebreak--, owner_domain,',
        ],
        'paletteOwner_address' => [
            'label' => '',
            'showitem' => 'owner_contact_address, owner_contact_address_supplement, --linebreak--, owner_contact_zip, owner_contact_city, --linebreak--, owner_contact_country,',
        ],
        'paletteOwner_communication' => [
            'label' => '',
            'showitem' => 'owner_contact_phone, owner_contact_fax, --linebreak--, owner_contact_email, owner_contact_office_hours,',
        ],
    ]
);
