<?php declare(strict_types=1);

/*
 * This file is part of the composer package buepro/typo3-easyconf.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use Buepro\Easyconf\Mapper\SiteConfigurationMapper;
use Buepro\Easyconf\Mapper\TypoScriptConstantMapper;

defined('TYPO3') or die('Access denied.');

// add columns and palettes
$GLOBALS['TCA']['tx_easyconf_configuration']['columns'] = array_replace_recursive(
    $GLOBALS['TCA']['tx_easyconf_configuration']['columns'],
    [
        'admPanel' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:admin_panel',
            'onChange' => 'reload',
            'config' => [
                'displayCond' => 'HIDE_FOR_NON_ADMINS',
                'type' => 'check',
                'renderType' => 'checkboxLabeledToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'labelChecked' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:justmaintain_activated',
                        'labelUnchecked' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:justmaintain_deactivated',
                    ]
                ],
            ],
            'tx_easyconf' => [
                'mapper' => TypoScriptConstantMapper::class,
                'path' => 'config.admPanel',
            ],
        ],
        'agency_name' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:agency_company',
            'displayCond' => [
                'AND' => [
                    'HIDE_FOR_NON_ADMINS',
                    'FIELD:easyconf_show_all_properties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'sitepackage.agency.name',
            ],
        ],
        'agency_link' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:agency_link',
            'displayCond' => [
                'AND' => [
                    'HIDE_FOR_NON_ADMINS',
                    'FIELD:easyconf_show_all_properties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'sitepackage.agency.link',
            ],
        ],
        'agency_phone' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:agency_phone',
            'displayCond' => [
                'AND' => [
                    'HIDE_FOR_NON_ADMINS',
                    'FIELD:easyconf_show_all_properties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'sitepackage.agency.phone',
            ],
        ],
        'agency_email' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:agency_email',
            'displayCond' => [
                'AND' => [
                    'HIDE_FOR_NON_ADMINS',
                    'FIELD:easyconf_show_all_properties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'sitepackage.agency.email',
            ],
        ],
        'agency_slogan' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:agency_slogan',
            'displayCond' => [
                'AND' => [
                    'HIDE_FOR_NON_ADMINS',
                    'FIELD:easyconf_show_all_properties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'sitepackage.agency.slogan',
            ],
        ],
        'agency_reference' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:agency_reference',
            'displayCond' => [
                'AND' => [
                    'HIDE_FOR_NON_ADMINS',
                    'FIELD:easyconf_show_all_properties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'text',
                'renderType' => 't3editor',
                'format' => 'html',
                'rows' => 1,
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'sitepackage.agency.reference',
            ],
        ],

    ]
);

$GLOBALS['TCA']['tx_easyconf_configuration']['palettes'] = array_replace_recursive(
    $GLOBALS['TCA']['tx_easyconf_configuration']['palettes'],
    [
        'paletteAdmin_settings' => [
            'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:palette_main_colors',
            'description' => 'LL: Header description',
            'showitem' => 'color_primary, color_secondary, color_tertiary, color_quaternary, --linebreak--, color_body-bg,',
        ],
        'paletteAgency' => [
            'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:palette_text_colors',
            'description' => 'LL: Agency description',
            'showitem' => 'agency_name, agency_link, --linebreak--, agency_phone, agency_email, --linebreak--, agency_slogan, --linebreak--, agency_reference',
        ],
    ]
);
