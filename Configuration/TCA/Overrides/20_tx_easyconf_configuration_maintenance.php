<?php

/*
 * This file is part of the composer package buepro/typo3-easyconf.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use Buepro\Easyconf\Mapper\SiteConfigurationMapper;

defined('TYPO3_MODE') || die();

// Register fields

$GLOBALS['TCA']['tx_easyconf_configuration']['columns'] = array_replace_recursive(
    $GLOBALS['TCA']['tx_easyconf_configuration']['columns'],
    [
        'justmaintain_active' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:justmaintain_active',
            'exclude' => 0,
            'onChange' => 'reload',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxLabeledToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'labelChecked' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:justmaintain_activated',
                        'labelUnchecked' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:justmaintain_deactivated',
                    ]
                ],
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'justmaintain_active',
                'valueMap' => [
                    0 => 'false',  // path.to.myField = false
                    1 => 'true',   // path.to.myField = true
                ],
            ],
        ],
        'justmaintain_maintenance_link' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:justmaintain_maintenance_link',
            'displayCond' => 'FIELD:justmaintain_active:=:1',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputLink',
                'linkPopup' => [
                    'options' => [
                        'blindLinkFields' => '',
                        'blindLinkOptions' => 'file,folder,spec,telephone'
                    ],
                ],
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'justmaintain_maintenance_link',
            ],
        ],
    ]
);

$GLOBALS['TCA']['tx_easyconf_configuration']['palettes'] = array_replace_recursive(
    $GLOBALS['TCA']['tx_easyconf_configuration']['palettes'],
    [
        'paletteMaintenance' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:palette_configuration',
            //'description' => 'LL: Configuration description',
            'showitem' => 'justmaintain_active, --linebreak--, justmaintain_maintenance_link',
        ],
    ]
);
