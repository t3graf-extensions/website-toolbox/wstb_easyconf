<?php

/*
 * This file is part of the composer package buepro/typo3-easyconf.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

$EM_CONF[$_EXTKEY] = [
    'title'            => 'Easyconf for Website Toolbox',
    'description'      => 'Integrates Roman Büchler\'s EXT:easyconf into the Ext:website_toolbox.

    Provides a module to easily configure main aspects from a website.

Once set up the module allows website owners without any TYPO3 knowledge to
configure main aspects from the website.',
    'category'         => 'module',
    'version'          => '1.5.0',
    'state'            => 'stable',
    'clearCacheOnLoad' => 1,
    'author'           => 'Roman Büchler',
    'author_email'     => 'rb@buechler.pro',
    'constraints'      => [
        'depends'   => [
            'typo3'                 => '11.5.0-11.5.99',
        ],
        'conflicts' => [],
    ],
    'autoload' => [
        'psr-4' => [
            'Buepro\\Easyconf\\' => 'Classes'
        ],
    ],
];
